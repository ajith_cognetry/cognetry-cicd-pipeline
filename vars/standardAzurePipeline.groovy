#!/usr/bin/groovy

def call(Map pipelineParams) {

	node('master') {
		stage('checkout and set agent') {
			checkout scm
			deleteDir()
		}
	}
	
	pipeline {
		
		agent any
		
		parameters {
			booleanParam(name: 'createRelease', defaultValue: false, description: 'Create a release from the repo branch (usually master)')
		}
		
		options {
			buildDiscarder logRotator(artifactDaysToKeepStr : '', artifactNumToKeepStr : '', daysToKeepStr : '', numToKeepStr : '10')
			disableConcurrentBuilds()			
		}
		
		stages {
			stage('prep build') {
				steps {
					echo "Deleting workspace..."
					deleteDir()
					
					echo "checking out repo...${WORKSPACE}"
					checkout scm
					
					sh 'mkdir -p deploy'
					dir('deploy') {
						git url: 'https://ajith_cognetry@bitbucket.org/ajith_cognetry/cognetry-deploy.git'
					}
					
					script {
						echo "Getting app data..."
						pomInfo = readMavenPom file: 'pom.xml'
						currentAppVersion = pomInfo.version
						currentAppGroupId = pomInfo.groupId
						currentAppArtifactId = pomInfo.artifactId						
						repoUrl = env.GIT_URL
						repoUrl = repoUrl.replace("https://","https://ajith_cognetry:xFyxzzagpNanNTRexZ9j@")
						repoBranch = "${env.BRANCH_NAME}".replaceAll('/|-','_')
						
						def build_props = readProperties file:"${WORKSPACE}/deploy/templates/build.properties"
						CONTAINER_REGISTRY = build_props['CONTAINER_REGISTRY']	
						AZ_USER = build_props['AZ_USER']
						AZ_TOKEN = build_props['AZ_TOKEN']
						PORT = build_props["${currentAppArtifactId}.port"]
						CONFIG_SERVICE_PASSWORD = build_props['CONFIG_SERVICE_PASSWORD']
						GIT_USER = build_props['GIT_USER']
						GIT_EMAIL = build_props['GIT_EMAIL']
						
						IMAGE_TAG = "${repoBranch}-${currentAppVersion}"
						
						MVN_OPTIONS = "-DCONTAINER_REGISTRY=${CONTAINER_REGISTRY} -DPORT=${PORT}"
						if(repoUrl.contains('cognetry-config-service.git')){
							MVN_OPTIONS = "${MVN_OPTIONS} -Djib.container.environment=CONFIG_SERVICE_PASSWORD=${CONFIG_SERVICE_PASSWORD}"
						}
						
					}
				}			
			}
			
			stage('create release') {
				when {
					allOf {
						branch 'master';
						environment name: "createRelease", value: 'true'
					}
				}
				steps {
					echo "Inside create Release"
					script {
						currentAppVersion = currentAppVersion.replace('-SNAPSHOT','')
						latestReleaseTag=sh (script: "git describe", returnStdout: true)
						echo "latestReleaseTag ${latestReleaseTag}"
						if(latestReleaseTag.length().compareTo(0) == 0){
							echo "No release tags found"
							newMinorVersion = '0'.toInteger()
						}
						else {
							latestReleaseTag = latestReleaseTag.split("-").first()
							echo "Release tags found ${latestReleaseTag}"
							newMinorVersion = latestReleaseTag.split("\\.").last().toInteger()+1
						}
						IMAGE_TAG = "${currentAppVersion}.${newMinorVersion}"
						echo "Creating release tags ${IMAGE_TAG}"
						
						sh "git config --global user.email '${GIT_EMAIL}'"
						sh "git config --global user.name '${GIT_USER}'"
						
						sh "git tag -a ${IMAGE_TAG} -m \"Auto: created release tag ${IMAGE_TAG}\" "
						sh "git push -v --tags --repo ${repoUrl}"
					}					
				}
			}
			
			
			stage('compile, image-build/ ACR push') {
				steps {
					script {
						sh "docker login cognetrytestcicdrepo.azurecr.io -u ${AZ_USER} -p ${AZ_TOKEN}"
						sh "mvn compile jib:build ${MVN_OPTIONS} -DIMAGE_TAG=${IMAGE_TAG} "
					}
				}
			}
			
			stage('Sonar build & Quaility Check') {
				steps {
					withSonarQubeEnv('sonarQube') {
						sh 'mvn clean package sonar:sonar'
					}
					sleep 60
					timeout(time: 5, unit: 'MINUTES') {
						waitForQualityGate abortPipeline: true
					}
				}
			}
			
			
			stage('prep deploy') {
				when { 
					branch 'dev_realm' 
				}
				
				steps {
					script {
						def dev_props = readProperties file:"${WORKSPACE}/deploy/templates/env-cicd.properties"
						NAMESPACE = dev_props['NAMESPACE']
						AKS_CLUSTER = dev_props['AKS_CLUSTER']
						RESOURCE_GROUP = dev_props['RESOURCE_GROUP']
						AZ_USER = dev_props['AZ_USER']
						AZ_TOKEN = dev_props['AZ_TOKEN']
						CONTAINER_REGISTRY = dev_props['CONTAINER_REGISTRY']
						
						echo "NAMESPACE is ${NAMESPACE}"
						echo "AKS_CLUSTER is ${AKS_CLUSTER}"
						echo "RESOURCE_GROUP is ${RESOURCE_GROUP}"
						echo "AZ_USER is ${AZ_USER}"
						echo "AZ_TOKEN is ${AZ_TOKEN}"
						echo "CONTAINER_REGISTRY is ${CONTAINER_REGISTRY}"
					}
				}			
			}
			
			
			stage('create yaml') {
				when { 
					branch 'dev_realm' 
				}
				steps {
					script {
						echo "Inside create yaml"		
						sh "cd ${WORKSPACE}/deploy/templates/${currentAppArtifactId} && export CONTAINER_REGISTRY=${CONTAINER_REGISTRY} && export IMAGE_TAG=${IMAGE_TAG} && envsubst < deployment.yaml > deployment_1.yaml"						
					}					
				}
			}
			
			stage('deploy') {
				when { 
					branch 'dev_realm' 
				}
				steps {
					script {
						echo "Inside deploy"						
						sh "az aks get-credentials --name ${AKS_CLUSTER} --resource-group ${RESOURCE_GROUP}"
						sh "kubectl config current-context"
						sh "kubectl apply -f ${WORKSPACE}/deploy/templates/${currentAppArtifactId}/deployment_1.yaml --namespace=${NAMESPACE} --validate=false"
					}
				}
			}
		
		}
		
		post {
			always {
				googlechatnotification(
					url: 'https://chat.googleapis.com/v1/spaces/AAAANrHqV00/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=h8DwCacb8bSEhix86YplEV4Q7Ky0_HFb4KawNg6lJyY%3D',
					message: "BuildNumber: ${env.BUILD_NUMBER} Build Status: ${currentBuild.result} JobName: ${env.JOB_NAME}\nUrl: ${env.BUILD_URL}",
					notifyAborted: true,
					notifyFailure: true,
					notifyNotBuilt: false,
					notifySuccess: true,
					notifyUnstable: true,
					notifyBackToNormal: true
				)
				deleteDir()
			}
		}
	
	}
	

}