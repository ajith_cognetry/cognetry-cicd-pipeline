#!/usr/bin/groovy

def call(Map pipelineParams) {

	node('master') {
		stage('checkout and set agent') {
			checkout scm
			deleteDir()
		}
	}
	
	pipeline {
		agent any
		
		parameters {
			booleanParam(name: 'createRelease', defaultValue: false, description: 'Create a release from the repo branch (usually master)')
		}
		
		options {
			disableConcurrentBuilds()
		}
		
		stages {
			stage('prep') {
				steps {
					echo "Deleting workspace..."
					deleteDir()
					
					echo "checking out repo..."
					checkout scm
					
					script {
						echo "Getting app data..."
						pomInfo = readMavenPom file: 'pom.xml'
						currentAppVersion = pomInfo.version
						currentAppGroupId = pomInfo.groupId
						currentAppArtifactId = pomInfo.artifactId
						
						repoUrl = env.GIT_URL
						echo "repoUrl is ${repoUrl}"
					}
				}			
			}
			
			
			stage('create Release') {
				when {
					allOf {
						environment name: "createRelease", value: 'true'
					}
				}
				steps {
					echo "Inside create Release"
				}
			}
			
			
			stage('compile') {
				steps {
					script {
						sh "mvn clean install"
					}
				}
			}
			
		
		}
	
	}
	

}