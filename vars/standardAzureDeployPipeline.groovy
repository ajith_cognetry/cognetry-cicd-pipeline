#!/usr/bin/groovy

def call(Map pipelineParams) {

	pipeline {
		agent any
		
		options {
			buildDiscarder logRotator(artifactDaysToKeepStr : '', artifactNumToKeepStr : '', daysToKeepStr : '', numToKeepStr : '10')
		}
		
		stages {
			stage('prep') {
				steps {
					echo "Deleting workspace..."
					deleteDir()
					
					echo "checking out repo..."
					checkout scm
					
					script {
						echo "artifactId is ${artifactId}"
						echo "appVersion is ${appVersion}"						
						echo "environment is ${environment}"					
						echo "WORKSPACE is ${WORKSPACE}"
						
						def props = readProperties file:"${WORKSPACE}/templates/env-${environment}.properties"
						NAMESPACE = props['NAMESPACE']
						AKS_CLUSTER = props['AKS_CLUSTER']
						RESOURCE_GROUP = props['RESOURCE_GROUP']
						AZ_USER = props['AZ_USER']
						AZ_TOKEN = props['AZ_TOKEN']
						CONTAINER_REGISTRY = props['CONTAINER_REGISTRY']	
						IMAGE_TAG = "${appVersion}"
						
						echo "NAMESPACE is ${NAMESPACE}"
						echo "AKS_CLUSTER is ${AKS_CLUSTER}"
						echo "RESOURCE_GROUP is ${RESOURCE_GROUP}"
						echo "AZ_USER is ${AZ_USER}"
						echo "AZ_TOKEN is ${AZ_TOKEN}"
						echo "CONTAINER_REGISTRY is ${CONTAINER_REGISTRY}"
					}
				}			
			}
			
			
			stage('create yaml') {
				when {
					allOf {
						environment name: "createYaml", value: 'true'
					}
				}
				steps {
					script {
						echo "Inside create yaml"		
						sh "cd ${WORKSPACE}/templates/${artifactId} && export CONTAINER_REGISTRY=${CONTAINER_REGISTRY} && export IMAGE_TAG=${IMAGE_TAG} && envsubst < deployment.yaml > deployment_1.yaml"						
					}					
				}
			}
			
			stage('deploy') {
				when {
					allOf {
						environment name: "deploy", value: 'true'
					}
				}
				steps {
					script {
						echo "Inside deploy"						
						sh "az aks get-credentials --name ${AKS_CLUSTER} --resource-group ${RESOURCE_GROUP}"
						sh "kubectl config current-context"
						sh "kubectl apply -f ${WORKSPACE}/templates/${artifactId}/deployment_1.yaml --namespace=${NAMESPACE} --validate=false"
					}
				}
			}
			
		
		}
		
		post {
			always {
				googlechatnotification(
					url: 'https://chat.googleapis.com/v1/spaces/AAAANrHqV00/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=h8DwCacb8bSEhix86YplEV4Q7Ky0_HFb4KawNg6lJyY%3D',
					message: "Deployment job for ${artifactId}_${appVersion} on ${environment} \nId: ${env.BUILD_NUMBER} Deployment Status: ${currentBuild.result} JobName: ${env.JOB_NAME}\nUrl: ${env.BUILD_URL}",
					notifyAborted: true,
					notifyFailure: true,
					notifyNotBuilt: false,
					notifySuccess: true,
					notifyUnstable: true,
					notifyBackToNormal: true
				)
				deleteDir()
			}
		}
	
	}
	

}